"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PositionShares = void 0;
const bignumber_js_1 = __importDefault(require("bignumber.js"));
class PositionShares {
    constructor(sharesIdentifier = '', // address of the receipt token, or if not receipt token, the address of the input token which will form the shares of the position
    amountAdded = (0, bignumber_js_1.default)(0), // amount of 'shares' added/removed in this transaction, it's positive if shares were added, negative if shares were removed
    sharePriceUsd = 0, // price of one share in USD
    positionBalance, // use this value for the position balance field if populated in positionValue
    isLiabilityPosition = false) {
        this.sharesIdentifier = sharesIdentifier;
        this.amountAdded = amountAdded;
        this.sharePriceUsd = sharePriceUsd;
        this.positionBalance = positionBalance;
        this.isLiabilityPosition = isLiabilityPosition;
    }
}
exports.PositionShares = PositionShares;
