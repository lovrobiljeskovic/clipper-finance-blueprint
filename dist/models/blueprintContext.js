"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlueprintContext = void 0;
class BlueprintContext {
    constructor(context, blueprintKey, userAddresses) {
        this.context = context;
        this.blueprintKey = blueprintKey;
        this.userAddresses = userAddresses;
    }
    getContext() {
        return this.context;
    }
}
exports.BlueprintContext = BlueprintContext;
