import { Blueprint } from '../src/blueprint';
import { BlockbydateAPI } from '../src/models/blockbydateAPI';
import { PositionValueContext } from '../src/models/positionValueContext';
import { RequestContext } from '../src/requestContext';
import { SimpleBlueprint } from '../src/simpleBlueprint';
import { DeepMocked, createMock } from '@golevelup/ts-jest';
import { beforeAll, expect, jest, test } from '@jest/globals';

describe('blueprint unit tests', () => {
  let blueprint: Blueprint;
  let context: RequestContext;
  let mockBlockByDateApi: BlockbydateAPI;

  beforeAll(() => {
    mockBlockByDateApi = createMock<BlockbydateAPI>({
      getBlockFromTimestamp(timestamp: number, networkId?: string): Promise<number> {
        return Promise.resolve(1);
      },
    });
    context = createMock<RequestContext>({
      getBlockByDateApi(): BlockbydateAPI {
        return mockBlockByDateApi;
      },
    });
    blueprint = new SimpleBlueprint(context);
  });

  it('is defined', () => {
    expect(blueprint).toBeDefined();
  });

  describe('blueprint implements required methods', () => {
    const timestamp = 123;
    const userAddresses = ['mock-user-1'];

    it('returns blueprint key', () => {
      expect(blueprint.getBlueprintKey()).toBeDefined();
    });
    it('getUserTransactions defined', async () => {
      const startingBlock = 1;
      const userAddresses = ['test-wallet-1']; // This should be edited by the BP developer
      const userTxns = await blueprint.getUserTransactions(context, userAddresses, startingBlock);
      expect(userTxns.lastSyncedBlock).toBeDefined();
      expect(userTxns.userTransactions).toBeDefined();
    });

    it('getPositionValue defined', async () => {
      const positionValueContext = new PositionValueContext(context, [], userAddresses, 'does-not-matter', timestamp);
      const userTxns = await blueprint.getPositionValue(positionValueContext);
      expect(userTxns.positionValueUsd).toBeDefined();
      expect(userTxns.tokenAmounts.length).toBeGreaterThanOrEqual(1);
      expect(userTxns.positionShareDetails.length).toBeGreaterThanOrEqual(1);
    });
  });
});
