import { RequestContext } from '../requestContext';
import { UserProtocolPositionSnapshotInterface } from './userProtocolPositionSnapshot.interface';

export class PositionValueContext {
  context: RequestContext;
  positionSnapshots: UserProtocolPositionSnapshotInterface[];
  userAddresses: string[];
  positionIdentifier: string;
  timestamp: number;

  constructor(
    context: RequestContext,
    positionSnapshots: UserProtocolPositionSnapshotInterface[],
    userAddresses: string[],
    positionIdentifier: string,
    timestamp: number,
  ) {
    this.context = context;
    this.positionSnapshots = positionSnapshots;
    this.userAddresses = userAddresses;
    this.positionIdentifier = positionIdentifier;
    this.timestamp = timestamp;
  }
}
