import { RequestContext } from '../requestContext';

export class BlueprintContext {
  constructor(public context: RequestContext, public blueprintKey: string, public userAddresses: string[]) {}

  getContext(): RequestContext {
    return this.context;
  }
}
