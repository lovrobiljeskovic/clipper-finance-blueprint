import { MetadataStore } from './meta/metadataStore';
import { PositionOperation } from './models/positionOperation';
import { PositionValue } from './models/positionValue';
import { PositionValueContext } from './models/positionValueContext';
import { TransactionDetails } from './models/transactionDetails';
import { UserTransactionResults } from './models/userTransactionResults';
import { RequestContext } from './requestContext';

export interface Blueprint {
  getContractName(): string;
  getBlueprintKey(): string;
  // This allows a child blueprint to be looked up via its parentID
  getParentBlueprintId(): string;
  getContext(): RequestContext;
  getUserTransactions(
    context: RequestContext,
    userAddresses: string[],
    fromBlock: number,
  ): Promise<UserTransactionResults>;
  classifyTransaction(context: RequestContext, txn: TransactionDetails): Promise<PositionOperation[]>;
  getPositionValue(positionContext: PositionValueContext): Promise<PositionValue>;
  getUserList(fromBlock: number): Promise<string[]>;
  syncMetadata(metadataStore: MetadataStore, lastSyncAt: number): Promise<number>; // returns last synced timestamp
  syncMetadataInterval(): number; // returns the number of seconds the system should sync after the last sync
}
